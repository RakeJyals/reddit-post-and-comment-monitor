import json #Read config file
import praw #Reddit api
from requests import Session #More reddit
from praw.models import MoreComments #For isinstance(comment)
import pytesseract #Pytesseract
import os #For removing files
from PIL import Image #Pytesseract
import urllib.request #Save image as file
from textblob import TextBlob #Sentiment analysis
import mysql.connector #SQL
import time #For collection period data
import calendar #For time stuff related to random subreddit collection
import detectlanguage #For making sure all subs are in english
import re #For removing non-alphabetic characters in detectSlurs

with open("config.json") as config_file: #All config presets
	config_json = json.load(config_file)

	#Reddit
	client_id = config_json["client_id"]
	client_secret = config_json["client_secret"]
	reddit_username = config_json["username"]
	reddit_password = config_json["password"]
	post_count = config_json["reddit_grabbed_posts"]
	comment_count = config_json["reddit_grabbed_comments"]
	subreddit_count = config_json["reddit_grabbed_subreddits"]

	#SQL
	host = config_json["sql_host"]
	user = config_json["sql_user"]
	passwd = config_json["sql_password"]
	database = config_json["sql_database"]
	content_table = config_json["sql_content_table"]
	content_var_list = config_json["sql_content_var_list"]
	subreddit_table = config_json["sql_subreddit_table"]
	subreddit_var_list_beginning = config_json["sql_subreddit_var_list_beginning"]
	subreddit_var_list_end = config_json["sql_subreddit_var_array_end"]

	#Other
	slur_list = config_json["slur_list"]
	image_formats = config_json["supported_image_formats"]
	kill_switch = config_json["kill_switch"]
	language_key = config_json["language_api_key"]


#Reddit and sql setup
session = Session()
reddit = praw.Reddit(client_id=client_id, client_secret=client_secret, username=reddit_username, password=reddit_password, requestor_kwargs={'session': session}, user_agent='testscript by testbot')

target_server = mysql.connector.connect(host=host, user=user, passwd=passwd, database=database) #Connects to sql server
cursor = target_server.cursor()

detectlanguage.configuration.api_key = language_key #For setting up lang detection api


def imageToString(url): #Returns string
	for format in image_formats:
		if format in url[len(url) - len(format):]: #Ensures link goes to compatible image, prevents errors
			return imageConvert(url)

	if "imgur" in url and "com/a" not in url and ".gifv" not in url: #Ensures imgur link is not a gifv or album, then changes url to direct image
		url.replace("//", "//i.")
		url += ".jpg"
		return imageConvert(url)

	if "gfycat" in url: #Changes gfycat url to one that directly links to the gif
		url.replace("//", "//giant.")
		url += ".gif"
		return imageConvert(url)

	return ""


def imageConvert(url): #Used only with imageToString for the sake of reapeating code, returns string
	print(url)
	try:
		urllib.request.urlretrieve(url, "temp_file.jpg") #Downloads file, saves to temp. Look into naming scheme to prevent overwriting
		converted_string = pytesseract.image_to_string(Image.open("temp_file.jpg")) #Converts image to text
		os.remove("temp_file.jpg")

	except:
		print("Error with pytesseract, not retrieving text")
		converted_string = ""
	return converted_string


def sentimentAnalysis(sample_text): #Returns tuple of polarity and subjectivity
	sample_text_blob = TextBlob(sample_text)
	return (sample_text_blob.sentiment.polarity, sample_text_blob.sentiment.subjectivity)


def detectSlurs(sample_text): #Returns boolean, looks for any slurs in the text
	for slur in slur_list: #Warning: algorithm is not designed for a large slur list size. Consider using database software instead
		if re.sub('[^A-Za-z]', '', slur.lower()) in re.sub('[^A-Za-z]', '', sample_text.lower()): #Changes comment to only contain lowercase letters to remove formatting issues
			return True
	return False


def storeBaseData(var_list, data_tuple_array, table): #Void, requires array of tuples to work, sends data to sql server
	literals = ""
	for x in range(len(data_tuple_array[0]) - 1): #Ensures correct number of literals are used
		literals += "%s, "
	literals += "%s"
	command = "insert into " + table + " (" + var_list + ") values (" + literals +  ")" #Sorry for the spaghetti, MAKE SURE THERE ARE AS MANY LITERALS AS COLUMNS
	cursor.executemany(command, data_tuple_array)
	target_server.commit() #Saves any changes


def updateBaseData(var_list, data_tuple_array, table, search_column, search_keyterm): #Void, 1 item at a time
	command = "update " + table + " set " #Creates sql command from scratch
	for var in var_list:
		command += var + " = %s, "
	command = command[:-2] + " where " + search_column + " = '" + search_keyterm + "'"
	cursor.execute(command, data_tuple_array)
	target_server.commit()


def getBaseData(table, search_column, search_keyterm, items = "*"): #Returns array of tuples, Gets all columns from table if keyterm is in column
	sql_search = (search_keyterm, )
	command = "select " + items + " from " + table + " where " + search_column + " = %s"
	cursor.execute(command, sql_search)
	return cursor.fetchall()


def collectPosts(subreddit_name): #Void, collects top number of desired posts from 1 specific subreddit
	print("Collecting posts from " + subreddit_name)
	item_list = []
	subreddit = reddit.subreddit(subreddit_name)
	for post in subreddit.hot(limit=post_count): #Pulls number of desired posts from "hot"
		if len(getBaseData(content_table, "reddit_id", post.id)) == 0: #Ensures post isn't already in database
			post_string = post.title + "\n" + post.selftext + "\n" + imageToString(post.url) #Collects all affiliated text
			sentiment = sentimentAnalysis(post_string)
			post_data = (post.id, subreddit_name, post_string, sentiment[0], sentiment[1], detectSlurs(post_string)) 
			#^Tuple format containing all sql info, needs actual text, polarity, subjectivity, and whether it contains slurs
			item_list.append(post_data)
			item_list.extend(getComments(post, subreddit_name))
	if len(item_list) > 0: #Ensures some data is going to be uploaded before trying
		storeBaseData(content_var_list, item_list, content_table)


def getComments(post, subreddit): #Returns an array of tuples containing top comments and properties for a post
	comment_list = []
	post.comment_sort = "Best" #Ensures "best" sorting algorithm is used
	post.comments.replace_more(limit=comment_count)
	for comment in post.comments.list():
		if (not isinstance(comment, MoreComments)) and comment.score > 0: #Ensures item is a comment and is not downvoted by community
			sentiment = sentimentAnalysis(comment.body)
			comment_data = (comment.id, subreddit, comment.body, sentiment[0], sentiment[1], detectSlurs(comment.body)) #Tuple containing sql info, needs same data as post_data
			comment_list.append(comment_data)
	return comment_list


def collectSubredditsBeginning(): #Void, collects all data associated with subreddits at the beginning of the collection process
	data_list = getBaseData(subreddit_table, "1", "1", "subreddit_name")
	subreddits = []
	for premade_subreddit in data_list: #Gets all subreddits already in database and updates data to be accurate
		subreddits.append(premade_subreddit[0])
		subreddit = reddit.subreddit(premade_subreddit[0])
		new_data = (subreddit.subscribers, len(subreddit.moderator()) / subreddit.subscribers)
		updateBaseData(subreddit_var_list_beginning.split(",")[1:], new_data, subreddit_table, "subreddit_name", premade_subreddit[0])

	while len(subreddits) < subreddit_count: #Ensures number of subreddits is desired
		subreddit = reddit.random_subreddit() #Gets a random subreddit, doesn't include nsfw ones
		posts = subreddit.new(limit=post_count)
		if not deadSub(posts, subreddit) and subreddit not in subreddits: #Ensures subreddit gets sufficient activity and isn't already in list
			subreddit_data = [(subreddit.display_name, subreddit.subscribers, len(subreddit.moderator()) / subreddit.subscribers)]
			data_list.append(subreddit_data)
			subreddits.append(subreddit)
			storeBaseData(subreddit_var_list_beginning, subreddit_data, subreddit_table) #Collects and uploads data


def collectSubredditsEnd(): #Void, collects all data associated with subreddit at the end for the sake of averaging out with beginning
	print("Collecting final data")
	data_list = []
	subreddits = getBaseData(subreddit_table, "1", "1", "subreddit_name, subscriber_count_beginning, moderators_per_subscriber_beginning")
	for subreddit_data in subreddits: #Goes through all subreddits
		subreddit = reddit.subreddit(subreddit_data[0])
		sub_count_end = subreddit.subscribers
		sub_growth = (sub_count_end - subreddit_data[1]) / subreddit_data[1]
		mod_count = len(subreddit.moderator()) / sub_count_end
		avg_polarity = getAvgNum(subreddit_data[0], "polarity")
		avg_subjectivity = getAvgNum(subreddit_data[0], "subjectivity")
		slur_rate = getAvgNum(subreddit_data[0], "slur_presence")
		vars = (subreddit.subscribers, sub_growth, mod_count, (mod_count + subreddit_data[2]) / 2, avg_polarity, avg_subjectivity, slur_rate)
		updateBaseData(subreddit_var_list_end, vars, subreddit_table, "subreddit_name", subreddit_data[0]) #Collects and uploads vars


def getAvgNum(subreddit, column): #Returns a float, calculates the average value of a specifc column, works with boolean to determine proportion
	total = 0
	num_list = getBaseData(content_table, "subreddit", subreddit, column) #Is array of tuples
	for num in num_list:
		total += num[0]
	return total / len(num_list)


def deadSub(posts, subreddit): #Returns boolean, checks criteria for compatible subreddits with research
	for post in posts:
		try:
			if time.mktime(time.localtime()) - post.created_utc > 21600 or detectlanguage.simple_detect(post.title) != "en": #3600 is 1 hour, 21600 is 6 hours
				#^Sees if newest posts are older than 1 hour or not in english
				print(post.subreddit.display_name + " is incompatible, not collecting")
				return True

		except: #Get error later
			print("Error with detectlanguage, avoiding")
			return True

	print("Adding " + subreddit.display_name + " to collection list")
	return False


def main(): #Void, this is the code that runs until the program is stopped by the kill switch var
	print("Starting new cycle")
	subreddits = getBaseData(subreddit_table, "1", "1", "subreddit_name, subscriber_count_beginning, moderators_per_subscriber_beginning")
	for subreddit in subreddits: #Cycle through all subreddits, collecting posts and comments
		collectPosts(subreddit[0])



collectSubredditsBeginning()
beginning_time = int(time.mktime(time.localtime())) #Make epoch, then convert on printout

while not kill_switch and time.gmtime(time.mktime(time.localtime()) - beginning_time)[1] < 2: #Will finish cycle before quitting
	main()
	with open("config.json") as config_file: #Updates kill switch var state
		config_json = json.load(config_file)
		kill_switch = config_json["kill_switch"]

collectSubredditsEnd()
end_time = int(time.mktime(time.localtime()))
run_time = time.gmtime(end_time - beginning_time)

print("Start time: " + str(time.localtime(beginning_time)[0:6]))
print("End time: " + str(time.localtime(end_time)[0:6]))
print("Run time: " + str(run_time[1:6]))
